package example;

import com.team254.lib.geometry.Pose2dWithCurvature;
import com.team254.lib.geometry.Rotation2d;
import com.team254.lib.trajectory.TimedView;
import com.team254.lib.trajectory.Trajectory;
import com.team254.lib.trajectory.TrajectoryIterator;
import com.team254.lib.trajectory.timing.TimedState;
import com.team254.lib.util.DriveSignal;

import ev3dev.actuators.lego.motors.EV3LargeRegulatedMotor;
import ev3dev.sensors.ev3.EV3GyroSensor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.robotics.SampleProvider;

public class Drive {
    private static Drive instance = null;
    public static Drive getInstance(){
        if(instance == null)
            instance = new Drive();
        return instance;
    }

    final EV3LargeRegulatedMotor left;
    final EV3LargeRegulatedMotor right;

    //final EV3GyroSensor gyro;
    //final SampleProvider gyroSamples;

    DriveMotionPlanner motionPlanner = new DriveMotionPlanner();

    public Drive(){
        left = new EV3LargeRegulatedMotor(MotorPort.A);
        right = new EV3LargeRegulatedMotor(MotorPort.C);

        left.resetTachoCount();
        right.resetTachoCount();

        /*gyro = new EV3GyroSensor(SensorPort.S2);
        gyro.reset();
        gyroSamples = gyro.getAngleMode();*/

        setVelocity(DriveSignal.NEUTRAL);
    }

    public enum State {
        OPEN_LOOP, PATH_FOLLOWING
    }
    State state = State.OPEN_LOOP;

    public void setVelocity(DriveSignal signal){
        DriveSignal finalSignal = signal;
        boolean leftForward = true;
        boolean rightForward = true;
        if(signal.getLeft() < 0.0){
            finalSignal = new DriveSignal(-signal.getLeft(), finalSignal.getRight());
            leftForward = false;
        }
        if(signal.getRight() < 0.0){
            finalSignal = new DriveSignal(finalSignal.getLeft(), -signal.getRight());
            rightForward = false;
        }
        left.setSpeed((int)Math.round(signal.getLeft()));
        right.setSpeed((int)Math.round(signal.getRight()));
        if(leftForward) left.forward();
        else left.backward();
        if(rightForward) right.forward();
        else right.backward();
    }

    public void setPath(Trajectory<TimedState<Pose2dWithCurvature>> path){
        motionPlanner.reset();
        motionPlanner.setTrajectory(new TrajectoryIterator<>(new TimedView<>(path)));
        state = State.PATH_FOLLOWING;
    }

    public void updatePathFollower(){
        double now = System.currentTimeMillis() / 1000.0;
        DriveMotionPlanner.Output output = motionPlanner.update(now, RobotState.getInstance().getFieldToVehicle(now));
        //Output is in radians/second
        setVelocity(new DriveSignal(Math.toDegrees(output.right_velocity), Math.toDegrees(output.left_velocity)));
        //System.out.println(left.getTachoCount() + "   " + right.getTachoCount());
    }

    public void update(){
        switch(state){
            case OPEN_LOOP:
            //no-op
            break;
            case PATH_FOLLOWING:
            updatePathFollower();
            break;
            default:
            break;
        }
    }

    public boolean isDone(){
        return state == State.PATH_FOLLOWING && motionPlanner.isDone();
    }

    public double encUnitsToInches(double encUnits){
        return (encUnits / Constants.kDriveEncoderRes) * (Math.PI * Constants.kDriveWheelDiameterInches);
    }

    public double encVelocityToInchesPerSec(double encVelocity){
        return encUnitsToInches(encVelocity);
    }

    public double inchesToEncUnits(double inches){
        return (inches / (Math.PI * Constants.kDriveWheelDiameterInches)) * Constants.kDriveEncoderRes;
    }

    public double inchesPerSecToEncVelocity(double inchesPerSec){
        return inchesToEncUnits(inchesPerSec);
    }

    public double getLeftEncoderDistance(){
        return encUnitsToInches(left.getTachoCount());
    }

    public double getRightEncoderDistance(){
        return encUnitsToInches(right.getTachoCount());
    }

    public double getLeftLinearVelocity(){
        return encVelocityToInchesPerSec(left.getRotationSpeed());
    }

    public double getRightLinearVelocity(){
        return encVelocityToInchesPerSec(right.getRotationSpeed());
    }

    public Rotation2d getHeading(){
        /*float[] sample = new float[gyroSamples.sampleSize()];
        gyroSamples.fetchSample(sample, 0);
        return Rotation2d.fromDegrees((double)sample[0]);*/
        return new Rotation2d();
    }
}