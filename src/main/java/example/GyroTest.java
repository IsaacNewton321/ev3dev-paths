package example;

import ev3dev.sensors.ev3.EV3GyroSensor;
import lejos.hardware.port.SensorPort;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

public class GyroTest {

    public static void main(final String[] args){
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            public void run() {
                System.out.println("Emergency Stop");
            }
        }));

        EV3GyroSensor gyro = new EV3GyroSensor(SensorPort.S2);

        SampleProvider sp = gyro.getRateMode();
		float[] sample = new float[sp.sampleSize()];
        sp.fetchSample(sample, 0);
        Delay.msDelay(500);
		sp = gyro.getAngleMode();
		sample = new float[sp.sampleSize()];
        sp.fetchSample(sample, 0);
        Delay.msDelay(500);
		sp = gyro.getAngleAndRateMode();
		sample = new float[sp.sampleSize()];
        sp.fetchSample(sample, 0);
        Delay.msDelay(500);
        //gyro.reset();
        sp = gyro.getAngleMode();

        for(int i = 0; i < 100; i++){
            sample = new float[sp.sampleSize()];
            sp.fetchSample(sample, 0);
            System.out.println("Gyro angle: " + sample[0]);
        }
    }
}