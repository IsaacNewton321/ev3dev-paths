package example;

import example.Kinematics;
import example.RobotState;
import com.team254.lib.geometry.Rotation2d;
import com.team254.lib.geometry.Twist2d;

public class RobotStateEstimator {
    static RobotStateEstimator instance_ = new RobotStateEstimator();
    private RobotState robot_state_ = RobotState.getInstance();
    private Drive drive_ = Drive.getInstance();
    private double left_encoder_prev_distance_ = 0.0;
    private double right_encoder_prev_distance_ = 0.0;
    private double back_encoder_prev_distance_ = 0.0;
    private boolean started = false;

    RobotStateEstimator() {
    }

    public static RobotStateEstimator getInstance() {
        return instance_;
    }

    public void update(){
        if(!started){
            left_encoder_prev_distance_ = drive_.getLeftEncoderDistance();
            right_encoder_prev_distance_ = drive_.getRightEncoderDistance();
            started = true;
        }
        final double left_distance = Math.abs(drive_.getLeftEncoderDistance());
        final double right_distance = Math.abs(drive_.getRightEncoderDistance());
        final double delta_left = left_distance - left_encoder_prev_distance_;
        final double delta_right = right_distance - right_encoder_prev_distance_;
        //System.out.println("Previous distances: (" + left_encoder_prev_distance_ + ", " + right_encoder_prev_distance_ + "), new distances: (" + left_distance + ", " + right_distance + ")");
        final Rotation2d gyro_angle = new Rotation2d();//drive_.getHeading();
        final Twist2d odometry_velocity = robot_state_.generateOdometryFromSensors(
                delta_left, delta_right, gyro_angle);
        final Twist2d predicted_velocity = Kinematics.forwardKinematics(drive_.getLeftLinearVelocity(),
                drive_.getRightLinearVelocity());
        robot_state_.addObservations(System.currentTimeMillis() / 1000.0, odometry_velocity,
                predicted_velocity);
        left_encoder_prev_distance_ = left_distance;
        right_encoder_prev_distance_ = right_distance;
    }
}

