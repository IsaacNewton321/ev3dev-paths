package example;

public class Constants {

    public static final double kDriveWheelDiameterInches = 2.1875; //inches
    public static final double kDriveWheelRadiusInches = kDriveWheelDiameterInches / 2.0;
    public static final double kDriveEncoderRes = 360.0;
    public static final double kDriveWheelTrackWidthInches = 5.75; //TODO measure
    public static final double kTrackScrubFactor = 1.0;

    // Tuned dynamics
    public static final double kRobotLinearInertia = 60.0;  // kg TODO tune 60.0
    public static final double kRobotAngularInertia = 10.0;  // kg m^2 TODO tune 10.0
    public static final double kRobotAngularDrag = 12.0;  // N*m / (rad/sec) TODO tune
    public static final double kDriveVIntercept = 1.055;  // V
    public static final double kDriveKv = 0.135;  // V per rad/s
    public static final double kDriveKa = 0.012;  // V per rad/s^2
    
    public static final double kPathKX = 4.0;  // units/s per unit of error
    public static final double kPathLookaheadTime = 0.4;  // seconds to look ahead along the path for steering
    public static final double kPathMinLookaheadDistance = 24.0;  // inches
    
}