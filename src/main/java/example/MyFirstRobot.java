package example;

import com.team254.lib.util.DriveSignal;

import lejos.utility.Delay;

public class MyFirstRobot {

    public static void main(final String[] args){
        Drive drive = Drive.getInstance();
        RobotStateEstimator stateEstimator = RobotStateEstimator.getInstance();
        RobotState robotState = RobotState.getInstance();
        TrajectoryGenerator generator = TrajectoryGenerator.getInstance();

        //To Stop the motor in case of pkill java for example
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            public void run() {
                System.out.println("Emergency Stop");
                drive.setVelocity(DriveSignal.NEUTRAL);
            }
        }));

        generator.generateTrajectories();
        drive.setPath(generator.getTrajectorySet().straightPath);

        long startTime = System.currentTimeMillis();
        int loops = 0;

        while(!drive.isDone()){
            stateEstimator.update();
            drive.update();
            System.out.println(robotState.getLatestFieldToVehicle().getValue().toString());
            loops++;
        }

        System.out.println("Path completed in: " + Double.toString((System.currentTimeMillis() - startTime) / 1000.0) + " seconds with " + loops + " loops.");
        System.out.println(robotState.getLatestFieldToVehicle().getValue().toString());

        System.exit(0);
    }
}
